const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const Schema = mongoose.Schema
const { createAlphanum } = require('tan-core/core/utils')

const InvoiceSchema = Schema({
	contractor: {
		type: String,
		required: true
	},
	price: {
		type: String,
		required: true
	},
	amount_paid: {
		type: String,
		required: true
	},
	issued_by: {
		type: String,
		required: true
	},
	timestamp: {
		type: Date,
		default: Date.now
	},
	invoice_id: {
		type: String,
		default: invoiceId
	},
	notes: {
		type: String
	}
}, {
	versionKey: false
})


function invoiceId() {
	const params = {
		parts: 3,
		length: 39
	}
	const uuid = createAlphanum(params)
	return uuid
}


module.exports = mongoose.model('Invoice', InvoiceSchema)