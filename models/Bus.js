const mongoose = require('mongoose')
mongoose.Promise = global.Promise
const Schema = mongoose.Schema

const BusSchema = Schema({
	busnumber: {
		type: String,
		unique: true,
		uppercase: true,
		trim: true
	},
	mobile_number: {
		type: String
	},
	imei: {
		type: String,
		unique: true,
		trim: true,
		uppercase: true
	},
	points: [
		{
			lat: {
				type: Number,
				// min: 0,
				// max: 90
			},
			long: {
				type: Number,
				// min: 0,
				// max: 90
			},
			timestamp: {
				type: Date,
				default: dateUTC
			},
			speed: {
				type: Number,
				default: 0,
				min: 0,
				max: 255
			},
			ignition: {
				type: Number
			}
		}
	]
}, {
	versionKey: false
})



function dateUTC() {
	const utc = new Date(Date.now()).toUTCString()
	return utc
}



module.exports = mongoose.model('Bus', BusSchema)