const Bus = require('./Bus')
const Invoice = require('./Invoice')

module.exports = {
	Bus, Invoice
}