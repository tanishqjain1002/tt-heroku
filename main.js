const dotenv = require('dotenv').load()
const express = require('express')
const app = express()
const http = require('http').Server(app)
const fs = require('fs')
const admin = require('firebase-admin')
const firebase_config = require('./config/firebase-admin.json')
const bodyParser = require('body-parser')
const ejs = require('ejs')
const cors = require('cors')
const haversine = require('haversine')
const mongoose = require('mongoose')
mongoose.connect(process.env.MONGO_URI)
const models = require('./models/combineModels')

const db = mongoose.connection
db.on('error', err => console.error(err))
db.once('open', () => console.log(`[MONGO] Connected to MongoDB`))

admin.initializeApp({
	credential: admin.credential.cert(firebase_config),
	databaseURL: "https://transport-tracker-82b00.firebaseio.com"
})

const user = admin.database().ref().child('/Users')
const time = admin.database().ref().child('/current-time')
const gps_locations = admin.database().ref().child('/gps_locations')
const bus_imei_mappers = admin.database().ref().child('/bus_imei_mappers')



///////////
//Routes //
///////////

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.set('view engine', 'ejs')


app.get('/', (req, res) => {
	res.json({
		version: require('./package.json').version,
		docs: require('./docs.json')
	})
})

app.get('/add', (req, res) => {
	res.render('add')
})

app.post('/addbus', (req, res) => {
	const { imei, busnumber } = req.body
	const Bus = models.Bus
	const obj = {}
	obj[imei] = busnumber

	bus_imei_mappers.update(obj)

	const obj1 = {}
	obj1[busnumber] = {
		imei,
		lat: 0,
		long: 0,
		speed: "0",
		ignition: 0,
		timestamp: new Date(Date.now()).toJSON()
	}

	gps_locations.update(obj1)

	new Bus({
		busnumber,
		imei,
		points: []
	})
	.save()
	.then(doc => res.json(doc))
	.catch(err => console.log(err))
})

app.post('/updatebusnumber', (req, res) => {
	const Bus = models.Bus
	const { imei, busnumber } = req.body

	Bus.findOne({
		imei
	})
	.then(doc => {
		if (doc.imei) {
			bus_imei_mappers.child(doc.imei).remove()
			gps_locations.child(doc.busnumber).remove()

			const obj = {}
			obj[doc.imei] = busnumber

			bus_imei_mappers.update(obj)

			const obj1 = {}
			obj1[busnumber] = {
				lat: 0,
				long: 0,
				ignition: 0,
				speed: "0",
				timestamp: new Date(Date.now()).toJSON(),
				imei
			}
			gps_locations.update(obj1)

			doc.busnumber = busnumber 
		}
		doc.save()
	})
	.catch(error => console.log(error))

	res.json("OK")
})

app.post('/updatemobilenumber', async (req, res) => {
	const Bus = models.Bus
	const { imei, mobilenumber } = req.body

	await Bus.findOne({
		imei
	})
	.then(doc => {
		if (doc.imei) {
			doc.mobile_number = mobilenumber 
		}
		doc.save()
	})
	.then(() => res.json("OK"))
	.catch(error => console.log(error))
})

app.post('/addinvoice', (req, res) => {
	const Invoice = models.Invoice
	const { contractor, price, amount_paid, issued_by, notes } = req.body

	new Invoice({
		contractor,
		price,
		amount_paid,
		issued_by,
		notes
	})
	.save()
	.then(doc => res.json(doc.invoice_id))
	.catch(error => console.log(error))
})

app.get('/addpoint/:imei', (req, res) => {
	const Bus = models.Bus
	const { lat, long, speed, busnumber, ignition } = req.query

	Bus.findOneAndUpdate({
		imei: req.params.imei
	}, {
		$push: {
			points: {
				lat,
				long,
				ignition,
				speed
			}
		}
	})
	.catch(err => console.log(err))

	res.json({
		status: 'OK'
	})
})

app.post('/updateignition', (req, res) => {
	const Bus = models.Bus
	const { imei, ignition } = req.body

	Bus.findOne({
		imei
	})
	.then(doc => {
		if(doc.points.length) {
			doc.points[doc.points.length-1].ignition = ignition
			doc.save()
		}
	})
	.catch(err => console.log(err))

	res.json({
		status: 'OK'
	})
})

app.get('/mobilecheck', (req, res) => res.json(true))

app.get("*", (req, res) => {
	res.redirect('/add')
})


http.listen(process.env.PORT, () => {
	console.log(`[SERVER] Server running on port ${process.env.PORT}`)
})

