/**
 * Converts <name>,<lat>,<long> to <lat>,<long>
 *
 * Use string split instead
 * 
 * @param  {String} string Location 
 * @return {String}        Lat and Long in <lat>,<long> format
 */
module.exports.location_parser = string => {
	let final = '';
	const first = string.indexOf(',');
	const second = string.lastIndexOf(',');

	for(let i=first+1;i<second;i++) {
		final += string[i];
	}
	final += ',';
	for(let i=second+1;i<string.length;i++) {
		final += string[i];
	}

	return final;
}















