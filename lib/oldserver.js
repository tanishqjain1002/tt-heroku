const dotenv = require('dotenv').load();
const express = require('express');
const app = express();
const http = require('http').Server(app);
const admin = require('firebase-admin');
const axios = require('axios');
const { location_parser } = require('./lib/string');
const firebase_config = require('./config/firebase-admin.json');
const mongojs = require('mongojs');
const $routes = mongojs('mongodb://admin:admin@ds119268.mlab.com:19268/tt-routes-map').collection('routes')
const distance_api_config = require('./config/distance_api.json');
const nodemailer = require('nodemailer')
const bodyParser = require('body-parser');
const cors = require('cors');
var holder = []

admin.initializeApp({
	credential: admin.credential.cert(firebase_config),
	databaseURL: "https://transport-tracker-82b00.firebaseio.com"
});


const bus_locations = admin.database().ref().child('/bus-locations');
const bus_position = admin.database().ref().child('/bus_position');
const pickup_points = admin.database().ref().child('/pickup-points');
const routes = admin.database().ref().child('/routes');
const user = admin.database().ref().child('/Users');
const user_ver = admin.database().ref().child('/user_ver');
const rawloc = admin.database().ref().child('/raw-locations');
const time = admin.database().ref().child('/current-time');



/////////////////////////////////////

rawloc.on('child_changed', snap => {
	holder.push({
		key: snap.ref.key,
		lat: snap.val().lat,
		lng: snap.val().lng
	});
});


//////////
// Mail //
//////////

const transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'transporttracker111@gmail.com',
		pass: 'transporttracker123'
	}
});

async function sendMail(to, payload, subject) {

	const mailOptions = {
		from: 'transporttracker111@gmail.com',
		to: to,
		subject: subject || 'Transport Tracker',
		text: JSON.stringify(payload)
	};

	try {
		transporter.sendMail(mailOptions, (error, info) => {
			if (error) {
				console.log(error);
			}
		});
	}
	catch (err) {
		console.log(err)
	}
}

/////////////////////////////
//Bus started notification //
/////////////////////////////

bus_position.on('child_changed', route => {
	if (route.val().flag_pos == 0 && route.val().isRunning == true) {
		user.once('value', userid => {
			for (const key in userid.val()) {
				if ((userid.val()[key].pickUpRouteId == route.ref.key) || (userid.val()[key].dropOffRouteId == route.ref.key)) {

					const payload = {
						data: {
							name: "Transport Tracker",
							type: "notification",
							number: "Bus has started",
							token: userid.val()[key].fcmToken
						}
					};

					console.log(`[SERVER] Sending Bus started for ${route.val().bus} to ${userid.val()[key].email}`);

					if (userid.val()[key].fcmToken) {
						admin.messaging().sendToDevice(userid.val()[key].fcmToken, payload)
							.catch(error => {
								console.log("Error sending message:", error);
							});
					}

					/**
					 * Send notification to users with pickup or dropoff point at 1
					 */
					if ((userid.val()[key].pickUpPointId == 1) || (userid.val()[key].dropOffPointId == 1)) {

						const payload = {
							data: {
								name: 'Transport Tracker',
								type: 'notification',
								number: 'Bus will reach soon',
								token: userid.val()[key].fcmToken
							}
						}

						console.log(`[SERVER] Sending Bus started for ${route.val().bus} to ${userid.val()[key].email}`);

						admin.messaging().sendToDevice(userid.val()[key].fcmToken, payload)
							.catch(error => {
								console.log(`Error while sending notification:`, error)
							})
					}
				}
			}

			try {
				sendMail('thatskevinjain@gmail.com', `${route.val().bus} started at ${new Date()}`, `${route.val().bus} started`)
				sendMail('akhilprm999@gmail.com', `${route.val().bus} started at ${new Date()}`, `${route.val().bus} started`)
				sendMail('abhi141997@gmail.com', `${route.val().bus} started at ${new Date()}`, `${route.val().bus} started`)
				sendMail('milindshah95@hotmail.com', `${route.val().bus} started at ${new Date()}`, `${route.val().bus} started`)
				sendMail('tanishqjain1002@gmail.com', `${route.val().bus} started at ${new Date()}`, `${route.val().bus} started`)
			}
			catch (error) {
				console.trace(error)
			}
		});
	}
});


///////////////////////////////
//User verified notification //
///////////////////////////////

/**
 * Add new user to /user_ver with property state false until verified initially when added
*/
user.on('child_added', newuser => {
	user.once('value', usr => {
		for (var key in usr.val()) {
			if ((usr.val()[key].email == 'thatskevinjain@gmail.com') || (usr.val()[key].email == 'akhilprm999@gmail.com')) {

				const payload = {
					data: {
						name: "Transport Tracker",
						type: "notification",
						number: "A new user signed up"
					}
				};

				admin.messaging().sendToDevice(usr.val()[key].fcmToken, payload)
					.catch((error) => {
						console.log("Error sending message:", error);
					});
			}
		}
	})
	user_ver.child(newuser.ref.key).set({
		email: newuser.val().email,
		state: false
	});
}, error => console.trace(error));


/**
 * Update /user_ver state property to true once /User has that specific contact and send notification to end user
 */
user.on('child_changed', snap => {
	user_ver.once('value', veruser => {

		if (snap.val().isActive == true && veruser.val()[snap.ref.key].state == false && snap.val().email == veruser.val()[snap.ref.key].email) {
			user_ver.child(snap.ref.key).update({
				state: true
			});

			const payload = {
				data: {
					name: "Transport Tracker",
					type: "notification",
					number: "Your account is verified",
					token: snap.val().fcmToken
				}
			};

			console.log(`[SERVER] Sending Verification to ${snap.val().email}`);
			admin.messaging().sendToDevice(snap.val().fcmToken, payload)
				.catch((error) => {
					console.log("Error sending message:", error);
				});
		}
	});
}, error => console.trace(error));

/////////////////////////////////////////
//Updating flag_pos for a specific bus //
/////////////////////////////////////////

bus_locations.on('child_changed', snapshot => {
	const node = {
		parent: snapshot.ref.key,
		lat: snapshot.val().lat,
		long: snapshot.val().lng,
		route_color: snapshot.val().route_color,
		route_id: snapshot.val().route_id,
		route_name: snapshot.val().route_name
	}

	function getRoutes() {
		return new Promise((resolve, reject) => {
			routes.once('value', (route_names) => {
				resolve(route_names);
			});
		});
	}

	function findRouteForBus(routes) {
		return new Promise((resolve, reject) => {
			const keys = Object.keys(routes.val())
			const vals = Object.values(routes.val())
			const filter = res => res.filter(Boolean);
			const res = vals.map((x, i) => (x.bus_name == node.parent ? i : null))

			if (keys[filter(res)[0]]) {
				resolve(keys[filter(res)[0]])
			} else {
				reject(false)
			}
		});
	}

	function getRouteData(route_name) {
		return new Promise((resolve, reject) => {
			bus_position.once('value', route => {
				resolve(route);
			});
		});
	}

	function getPickupPoints(route_name) {
		return new Promise((resolve, reject) => {
			pickup_points.once('value', points => {
				resolve(Object.values(points.child(route_name).val()));
			});
		});
	}

	function makeDestinationString(flag, pickup_points) {
		return new Promise((resolve, reject) => {
			let dest = '';
			for (let i = flag; i < pickup_points.length; i++) {
				dest += location_parser(pickup_points[i]) + '|';
			}
			dest = dest.slice(0, dest.length - 1);
			resolve(dest);
		});
	}

	function makeApiCall(dest) {
		return new Promise((resolve, reject) => {
			axios.get(`https://maps.googleapis.com/maps/api/distancematrix/json?origins=${node.lat},${node.long}&destinations=${dest}&key=${distance_api_config.key}`)
				.then(({ data }) => {
					resolve(data);
				});
		});
	}

	function sendNotification(route_name, flag) {
		return new Promise((resolve, reject) => {
			user.once('value', usr => {
				for (const key in usr.val()) {
					if ((usr.val()[key].pickUpRouteId == route_name && usr.val()[key].pickUpPointId == flag) || (usr.val()[key].dropOffRouteId == route_name && usr.val()[key].dropOffPointId == flag)) {

						const payload = {
							data: {
								name: "Transport Tracker",
								type: "notification",
								number: "Bus will be reaching soon",
								token: usr.val()[key].fcmToken
							}
						};
						console.log("[SERVER] Sending bus will reach soon to " + usr.val()[key].fcmToken);
						admin.messaging().sendToDevice(usr.val()[key].fcmToken, payload)
							.catch(error => console.trace(error));
					}
				}
			});
		});
	}

	function updateFlagPosition(distandtime, flag, route_name) {
		return new Promise((resolve, reject) => {
			let nextflag;
			for (let i = 1; i < distandtime.length; i++) {
				if (distandtime[i - 1].distance.value <= 100) {
					bus_position.child(route_name.toString()).update({
						flag_pos: flag + i,
						bus: node.parent
					});

					nextflag = flag + i + 1;

					console.log(`[SERVER] Updated flag_pos for ${node.parent} on ${route_name} from ${flag} to ${flag + i}`);
					resolve(nextflag);
					break;
				}
			}
		});
	}

	async function init() {

		/**
		 * All routes from /routes
		 * @type {Object}
		 */
		const routes = await getRoutes().catch(error => console.trace(error));

		/**
		 * Route Name
		 * @type {String}
		 */
		const route_name = await findRouteForBus(routes).catch(err => { return err });

		if (route_name) {

			/**
			 * Data for route bus is running on
			 * @type {Object}	
			 * @property {String} bus  		Bus name
			 * @property {String} flag_pos	Flag Position 
			 */
			const route_data = await getRouteData(route_name).catch(error => console.trace(error));

			/**
			 * Flag position of bus on particular route
			 * @type {Number}
			 */
			const flag = route_data.child(route_name).val().flag_pos;

			/**
			 * List of all Pickup points with respective route
			 * @type {Array}
			 */
			const pickup_points = await getPickupPoints(route_name).catch(error => console.trace(error));

			/**
			 * Destination string from current flag position to end
			 * @type {String}
			 */
			const dest = await makeDestinationString(flag, pickup_points).catch(error => console.trace(error));

			/**
			 * Get Data from Google Distance Matrix API
			 * @type {Object}
			 * @property {String} 	status 					Status
			 * @property {Array} 	destination_addresses	Destinations from current flag position
			 * @property {Array} 	origin_addresses		Origins
			 * @property {Array} 	rows[0].elements 		distance and time Data			
			 */
			const api_data = await makeApiCall(dest).catch(error => console.trace(error));


			if ('elements' in api_data.rows[0]) {
				/**
				 * Distance and Time from Google Distance matrix API
				 * @type {Array}
				 */
				const distandtime = api_data.rows[0].elements;

				/**
				 * Update Flag Position and return the next flag pos
				 * @type   {Number|undefined} 
				 * @param {Array} 				distandtime 	Distance and Time
				 * @param {Number}				flag   			Flag position of Bus
				 * @param {String} 				route_name 		Route name
				 * @param {String} 				route_data.bus 	Bus name
				 */
				const nextflag = await updateFlagPosition(distandtime, flag, route_name).catch(error => console.trace(error));

				if (nextflag) {
					/**
					 * Send notifiocation to all users which have pickup points on the next flag
					 */
					await sendNotification(route_name, nextflag).catch(error => console.trace(error))
				}
			}
		}
	}
	init()
}, error => console.trace(error));


/////////
// GCP //
/////////

function url(lat, long) {
	return `https://roads.googleapis.com/v1/snapToRoads?path=${lat},${long}&interpolate=true&key=${distance_api_config.key}`
}

function apicall(lat, long, key) {
	return new Promise((resolve, reject) => {
		axios.get(url(lat, long))
			.then(({ data }) => {
				console.log(`[SERVER] Sent Request for ${key} with ${lat},${long}`);
				if (data.snappedPoints) {
					resolve({
						points: data.snappedPoints
					})
				} else {
					reject(new Error(`[SERVER] No Response from Google for ${key}`))
				}
			})
	})
}

function indianDate() {
	const d = new Date();
	return new Date(d.getTime() + (330 + d.getTimezoneOffset()) * 60000).toString();
}

function busupdate(lat, long, key) {
	return new Promise((resolve, reject) => {
		console.log(`[SERVER] Updated bus-locations for ${key} with ${lat},${long}`);
		bus_locations.child(key).update({
			lat: lat,
			lng: long
		});
		resolve()
	})
}

/**
 * Gets the route name for a specific bus
 * @param  {String} bus bus name
 * @return {String}     route name
 */
function getRoute(bus) {
	return new Promise((resolve, reject) => {
		routes.once('value', snap => {
			const vals = Object.values(snap.val())
			const index = Object.keys(snap.val())

			vals.forEach((element, i, self) => {
				if (element.bus_name == bus) {
					resolve(index[i])
				}
			})
		})
	})
}

function checkPickupPoints(routename) {
	return new Promise((resolve, reject) => {
		pickup_points.once('value', snap => {
			const index = Object.keys(snap.val())
			const status = index.filter(x => x == routename)[0]

			if (status) {
				resolve(status)
			}
			else if (!status) {
				reject(false)
			}
		})
	})
}

function findLoggedRoute(bus, route) {
	return new Promise((resolve, reject) => {
		$routes.findOne({
			busName: bus,
			routeName: route
		}, (err, docs) => {
			if (err) throw err;
			if (docs) {
				resolve(true)
			} else {
				resolve(false)
			}
		})
	})
}

function addPosToExistingRoute(key, routename, lat, long) {
	return new Promise((resolve, reject) => {
		console.log(`[SERVER] Added ${lat},${long} in logs for ${key} on ${routename}`)
		$routes.update({
			busName: key,
			routeName: routename
		}, {
				$push: {
					points: {
						lat: lat,
						long: long,
						timestamp: indianDate()
					}
				}
			}, (err, docs) => {
				if (err) throw err;
				if (docs) resolve()
			})
	})
}

function createNewRoute(key, routename, lat, long) {
	return new Promise((resolve, reject) => {
		console.log(`[SERVER] No route found for ${key}. Logging started...`)
		$routes.insert({
			busName: key,
			routeName: routename,
			points: [
				{
					lat: lat,
					long: long,
					timestamp: indianDate()
				}
			]
		}, (err, docs) => {
			if (err) throw err;
			if (docs) resolve()
		})
	})
}

setInterval(async () => {
	if (holder.length > 0) {
		let clone = holder;
		holder = [];
		clone = clone.reverse().filter((thing, index, self) => index == self.findIndex((t) => (t.key == thing.key)))
		const length = clone.length;

		for (let i = 0; i < length; i++) {
			const key = clone[i].key

			const data = await apicall(clone[i].lat, clone[i].lng, key)
				.catch(err => { console.trace(err); return false });

			if (data) {
				const routeName = await getRoute(key)
					.catch(error => console.trace(error))

				const pickUpPoint = await checkPickupPoints(routeName)
					.catch(error => { return error })

				if (!pickUpPoint) {
					const serverRouteCheck = await findLoggedRoute(key, routeName)
						.catch(error => console.trace(error))

					if (serverRouteCheck) {
						await addPosToExistingRoute(key, routeName, data.points[0].location.latitude, data.points[0].location.longitude)
							.catch(error => console.trace(error))
					} else {
						await createNewRoute(key, routeName, data.points[0].location.latitude, data.points[0].location.longitude)
							.catch(error => console.trace(error))
					}
				}

				await busupdate(data.points[0].location.latitude, data.points[0].location.longitude, key)
					.catch(error => console.trace(error))
			}
		}
	}
}, 15000);



//////////
// Time //
//////////

setInterval(() => {
	const now = Date.now();
	time.update({
		moment: now
	});
}, 1000);

///////////
//Routes //
///////////

app.use(express.static(__dirname))
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.json({
		version: require("./package.json").version,
		name: 'Transport Tracker Server',
		changelog: 'Added socket io test',
		api: [
			{
				path: "/notification/{fcm}",
				desc: "Send notification using firebase to client apps"
			},
			{
				path: "/email",
				type: "POST",
				desc: "Instant email",
				payload: {
					to: "string",
					data: "strictly string of any length",
					subject: "optional string"
				}
			},
			{
				path: "/route/bybus/:busname",
				type: "GET",
				desc: "Get data for a bus with bus name from logs"
			},
			{
				path: "/route/byroute/:busname",
				type: "GET",
				desc: "Get data for a bus with route name from logs"
			},
			{
				path: "/customnotification",
				type: "POST",
				desc: "Custom Notifications",
				payload: {
					type: "string or number",
					data: "string",
					title: "string",
					extra: "string",
					fcm: "string"
				}
			}
		]
	});
});


app.get("/notification/:token", (req, res) => {

	const payload = {
		data: {
			name: "Transport Tracker",
			type: "notification",
			number: "Your account is verified",
			token: req.params.token
		}
	};

	admin.messaging().sendToDevice(req.params.token, payload)
		.then(response => res.json(response))
		.catch(error => res.json(error));
});

app.post('/customnotification', (req, res) => {
	const type = req.body.type
	const data = req.body.data
	const title = req.body.title
	const extra = req.body.extra
	const fcm = req.body.fcm

	const payload = {
		data: { fcm, type, data, title, extra }
	}

	admin.messaging().sendToDevice(fcm, payload)
		.then(response => res.json(response))
		.catch(error => res.json(error));
})


app.post('/email', (req, res) => {
	try {
		sendMail(req.body.to, req.body.data, req.body.subject)
	}
	catch (err) {
		res.status(400).send();
	}
	finally {
		res.status(204).send();
	}
});

app.get('/route/bybus/:number', (req, res) => {
	$routes.findOne({
		busName: req.params.number
	}, (err, docs) => {
		if (err) throw err;
		if (docs) {
			delete docs._id
			res.json(docs)
		} else {
			res.json({})
		}
	})
})

app.get('/route/byroute/:number', (req, res) => {
	$routes.findOne({
		routeName: req.params.number
	}, (err, docs) => {
		if (err) throw err;
		if (docs) {
			delete docs._id
			res.json(docs)
		} else {
			res.json({})
		}
	})
})

http.listen(process.env.PORT, () => {
	console.log(`[SERVER] Server running on port ${process.env.PORT}`);
});

